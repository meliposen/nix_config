  services.openssh = {
    enable = true;
    openFirewall = true;
    permitRootLogin = "no";
    passwordAuthentication = false;
  };

  users.users.mel.openssh.authorizedKeys.keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEsKbNyQd09gLTHhi32996c5HSq4ELxQYJteYCkQ4jvA"];

